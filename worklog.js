const workLog = ()=>{
	window.addEventListener('load', ()=>{
		eventListener();
	});
};


const waitForElement = (elem, callback)=>{
    setTimeout(()=>{
        if(document.querySelector(elem)) {
            callback(null, true);
        } else {
            waitForElement(elem, callback);
        }
    }, 100);  
};
const fillTheForm = ()=> {
    let description = document.querySelector('#tempo-issue-picker-popup-field').value;
    document.querySelector('#comment-popup').value = description;
    document.querySelector('#popup_ABC_').value = 'B50';
    document.querySelector('#popup_Type_').value = 'JS';
    let timeInput = document.querySelector('#time-popup');
    timeInput.value = '2h';
    timeInput.selectionStart = 0;
    timeInput.selectionEnd = 2;
    
};

const eventListener = ()=>{
    window.addEventListener('keypress', (event)=>{
		if(event && event.which == 119 && 
			document.activeElement.tagName.toLowerCase() != 'input' && document.activeElement.tagName.toLowerCase() != 'textarea') {
			waitForElement('#timeSpentSeconds', (err, res)=>{
			   if (err) {
				   console.error(err);
			   } else {
				   fillTheForm();
			   }
		   });
		}
	});
};
workLog();