 // ==UserScript==
// @name         Task Unwatch
// @namespace
// @version      0.2
// @description  Open your Jira board, and in the header next to the Create button, there will be an Unwatch button. If you click that, you wont watch the currently visible tasks.
// @description  If you want to unwatch all tasks, remove all jira filters, open all stories, and then click the Unwatch button.
// @author       Bálint Ádám (pucuka)
// @match        https://jira.shiwaforce.com/secure/RapidBoard.jspa*
// @grant        none
// ==/UserScript==

(function() {
	const UNWATCH_BUTTON_TEXT = 'Unwatch';
	const UNWATCH_API_METHOD = 'DELETE';
	const JIRA_BOARD_TASK_SELECTOR = '.js-detailview.ghx-issue';
	const JIRA_TASK_HIDDEN_ID_ATTRIBUTE = 'data-issue-id';
	const PRIMARY_HEADER_SELECTOR = '.aui-header-primary';
	const PRIMARY_HEADER_LIST_SELECTOR = 'ul.aui-nav';
	window.addEventListener('load', ()=> {
		let unsubscribeListItem = document.createElement('li');
		let unsubscribeListItemLink = document.createElement('a');
		unsubscribeListItemLink.innerHTML = UNWATCH_BUTTON_TEXT;
		unsubscribeListItemLink.setAttribute('href', '#');
		unsubscribeListItem.appendChild(unsubscribeListItemLink);
		unsubscribeListItemLink.addEventListener('click', (event)=>{
			unsubscribe();
			event.preventDefault();
		});
		document.getElementById('header').querySelector('nav').querySelector(PRIMARY_HEADER_SELECTOR).querySelector(PRIMARY_HEADER_LIST_SELECTOR).appendChild(unsubscribeListItem);
		function unsubscribe() {
			document.querySelectorAll(JIRA_BOARD_TASK_SELECTOR).forEach(task=> {
				let taskHiddenId = task.getAttribute(JIRA_TASK_HIDDEN_ID_ATTRIBUTE);
				if (taskHiddenId) {
					fetch(`https://jira.shiwaforce.com/rest/api/1.0/issues/${taskHiddenId}/watchers`, {method: UNWATCH_API_METHOD}).then(()=>{
						task.style.border = '3px solid green';
					}).catch(()=>{
						task.stlye.border = '3px solid red';
					});
				}
			});
		}
	});
})();