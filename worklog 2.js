// ==UserScript==
// @name         Jira worklog kitolto script
// @version      0.6
// @description  Nyomd meg a W-t egy taszkon, és a modalban ki lesznek töltve a kőtelező mezők automatikusan. Az első 5 const értelemszerűen átírhatő a scriptben.
// @author       Balint Adam (pucuka)
// @match        https://jira.shiwaforce.com/*
// @grant        none
// ==/UserScript==

(function () {
	const WORK_TYPE = 'JS';
	const WORK_TIME_SPENT = '2h';
	const WORK_ABC_TYPE = 'B50';
	const OUT_COORDINATES = [{
		latitude: 47.547750,
		longitude: 19.073874,
		name: "OTP Babér utca"
	}];
	const IS_WORK_OUT = false; // fallback, if geolocation is not available

	const WORK_OUT = 'Yes';
	const TEMPO_IFRAME_SELECTOR = '.tempo-iframe';
	const TEMPO_FORM_TITLE_SELECTOR = 'div[title][class]';
	const TEMPO_FORM_SELECTOR = '#worklogForm';
	const TEMPO_DESCRIPTOPN_FIELD_SELECTOR = '#comment';
	const TEMPO_TIME_SPENT_SELECTOR = '#timeSpentSeconds';
	const TEMPO_ABC_TYPE_SELECTOR = '#_ABC_';
    const TEMPO_ABC_TYPE_INPUT_SELECTOR = '#react-select-2-input';
	const TEMPO_ABC_TYPE_SELECTED_OPTION_SELECTOR = '[id^="react-select-2-option-"]';
	const TEMPO_SELECT_FOCUSED_SELECTOR = '.Select-option.is-focused';
	const TEMPO_TYPE_SELECTOR = '#react-select-3-input';
	const TEMPO_TYPE_SELECTED_OPTION_SELECTOR = '[id^="react-select-3-option-"]';
	const TEMPO_OUT_SELECTOR = '#react-select-6-input';
	const TEMPO_OUT_LABEL_SELECTOR = 'label[for="_Out_"]';
	const TEMPO_OUT_SELECTED_OPTION_SELECTOR = '[id^="react-select-6-option-"]';
	const OPEN_TEMPO_KEYPRESS_KEY = 119; // W
	const PAGE_TYPE_IFRAME = 'iframe';
	const PAGE_TYPE_DOCUMENT = 'document';
	const TEMPO_DOCUMENT_SELECTOR = '#tempo-global-dialog-bundled';
	const COORDINATES_PRECISION = 0.002;

	const workLog = () => {
		window.addEventListener('load', () => {
			openTempoEventListener();
		});
	};

	const isCoordinatesInRange = (center, userCoordinates, precision) => {
		return center.latitude + precision >= userCoordinates.latitude && center.latitude - precision <= userCoordinates.latitude &&
			center.longitude + precision >= userCoordinates.longitude && center.longitude - precision <= userCoordinates.longitude;
	};

	const getCoordinatesFromArray = (userCoordinates, array) => {
		return array.find((item) => {
			return isCoordinatesInRange(item, userCoordinates, COORDINATES_PRECISION);
		});
	};

	const openTempoEventListener = () => {
		window.addEventListener('keypress', async (event) => {
			if (event && event.which == OPEN_TEMPO_KEYPRESS_KEY &&
				document.activeElement.tagName.toLowerCase() != 'input' && document.activeElement.tagName.toLowerCase() != 'textarea') {
				let tempoTitlePromise = waitForElement(TEMPO_FORM_TITLE_SELECTOR, TEMPO_IFRAME_SELECTOR, TEMPO_DOCUMENT_SELECTOR);
				let tempoAbcPromise = waitForElement(TEMPO_ABC_TYPE_SELECTOR, TEMPO_IFRAME_SELECTOR, TEMPO_DOCUMENT_SELECTOR);
				let tempoCommentPromise = waitForElement(TEMPO_DESCRIPTOPN_FIELD_SELECTOR, TEMPO_IFRAME_SELECTOR, TEMPO_DOCUMENT_SELECTOR);
				let allResult = await Promise.all([tempoTitlePromise, tempoAbcPromise, tempoCommentPromise]);
				fillTheForm(allResult[0]);
			}
		});
	};

	const waitForElement = (elem, iframeElem, documentElem) => {
		return new Promise((resolve) => {
			setTimeout(() => {
				const elemExists = elemExistsInIframe(elem, iframeElem, documentElem);
				if (elemExists) {
					return resolve(elemExists);
				} else {
					return resolve(waitForElement(elem, iframeElem, documentElem));
				}
			}, 100);
		});
	};

	const elemExistsInIframe = (elem, iframeElem, documentElem) => {
		if (document.querySelector(iframeElem) && document.querySelector(iframeElem).contentWindow.document.querySelector(elem)) {
			return PAGE_TYPE_IFRAME;
		} else if (document.querySelector(documentElem) && document.querySelector(documentElem).querySelector(elem)) {
			return PAGE_TYPE_DOCUMENT;
		} else {
			return false;
		}
	};

	// https://stackoverflow.com/questions/30683628/react-js-setting-value-of-input
	const setNativeValue = (element, value) => {
		let lastValue = element.value;
		element.value = value;
		let event = new Event("input", {
			target: element,
			bubbles: true
		});
		// React 15
		event.simulated = true;
		// React 16
		let tracker = element._valueTracker;
		if (tracker) {
			tracker.setValue(lastValue);
		}
		element.dispatchEvent(event);
	};

	const triggerMouseEvent = (node, eventType) => {
		var clickEvent = document.createEvent('MouseEvents');
		clickEvent.initEvent(eventType, true, true);
		node.dispatchEvent(clickEvent);
	};

	const forceBlurEvent = (element) => {
		let event = new Event("blur", {
			target: element,
			bubbles: true
		});
		event.simulated = true;
		element.dispatchEvent(event);
	};

	const fillOutField = (currentWindow) => {
		let outInput = currentWindow.document.querySelector(TEMPO_OUT_SELECTOR);
		setNativeValue(outInput, WORK_OUT);
		let outOption = currentWindow.document.querySelector(TEMPO_OUT_SELECTED_OPTION_SELECTOR) || currentWindow.document.querySelector(TEMPO_SELECT_FOCUSED_SELECTOR);
		triggerMouseEvent(outOption, 'click');
	};

	const fillTheForm = (type) => {
		// 'description' field
		let currentWindow = type == PAGE_TYPE_IFRAME ? document.querySelector(TEMPO_IFRAME_SELECTOR).contentWindow : window;
		let form = currentWindow.document.querySelector(TEMPO_FORM_SELECTOR);
		let taskNameElem = form.querySelector(TEMPO_FORM_TITLE_SELECTOR);
		let taskName = taskNameElem.getAttribute('title');
		let descriptionElem = form.querySelector(TEMPO_DESCRIPTOPN_FIELD_SELECTOR);
		setNativeValue(descriptionElem, taskName);

		// 'worked' field
		let workedInput = currentWindow.document.querySelector(TEMPO_TIME_SPENT_SELECTOR);
		setNativeValue(workedInput, WORK_TIME_SPENT);
		forceBlurEvent(workedInput);

		// 'abc' field
		let abcInput = currentWindow.document.querySelector(TEMPO_ABC_TYPE_INPUT_SELECTOR);
		setNativeValue(abcInput, WORK_ABC_TYPE);
		let option = currentWindow.document.querySelector(TEMPO_ABC_TYPE_SELECTED_OPTION_SELECTOR) || currentWindow.document.querySelector(TEMPO_SELECT_FOCUSED_SELECTOR);
		triggerMouseEvent(option, 'click');

		// 'type' field
		let typeInput = currentWindow.document.querySelector(TEMPO_TYPE_SELECTOR);
		setNativeValue(typeInput, WORK_TYPE);
		let typeOption = currentWindow.document.querySelector(TEMPO_TYPE_SELECTED_OPTION_SELECTOR) || currentWindow.document.querySelector(TEMPO_SELECT_FOCUSED_SELECTOR);
		triggerMouseEvent(typeOption, 'click');

		// 'out' field
		if (OUT_COORDINATES.length) {
			navigator.geolocation.getCurrentPosition(position => {
				const outLocation = getCoordinatesFromArray(position.coords, OUT_COORDINATES);
				if (outLocation) {
					fillOutField(currentWindow);
					const outLabel = currentWindow.document.querySelector(TEMPO_OUT_LABEL_SELECTOR);
					if (outLabel) {
						let infoDiv = document.createElement('div');
						infoDiv.innerHTML = `Látlak, itt vagy: ${outLocation.name}`;
						outLabel.parentNode.parentNode.parentNode.appendChild(infoDiv);
					}
				}
			}, () => {
				// if geolocation is not available
				if (IS_WORK_OUT) {
					fillOutField(currentWindow);
				}
			});
		} else if (IS_WORK_OUT) {
			fillOutField(currentWindow);
		}

	};

	workLog();
})();
